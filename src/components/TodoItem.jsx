import { useState } from 'react'
import { FaTrash } from 'react-icons/fa'
import { AiFillEdit } from 'react-icons/ai'
import { MdOutlineCancel } from 'react-icons/md'
import { MdUpdate } from 'react-icons/md'

const TodoItem = ({ itemData, setTodos }) => {
  const [editing, setEditing] = useState(false)
  const [title, setTitle] = useState(itemData.title)

  const handleEditing = () => {
    setEditing(true)
  }

  const handleUpdate = () => {
    setTodos((prevState = []) =>
      prevState.map((todo) => {
        if (todo.id === itemData.id) {
          return {
            ...todo,
            title,
          }
        }
        return todo
      })
    )
    setEditing(false)
  }

  const handleCancel = () => {
    setTitle(itemData.title)
    setEditing(false)
  }

  const handleChange = (id) => {
    setTodos((prevState = []) =>
      prevState.map((todo) => {
        if (todo.id === id) {
          return {
            ...todo,
            completed: !todo.completed,
          }
        }
        return todo
      })
    )
  }

  const handleDelete = (id) => {
    setTodos((prevState = []) => prevState.filter((todo) => todo.id !== id))
  }

  return (
    <div className='flex items-center justify-between bg-white shadow-lg rounded-lg mb-4 p-4'>
      {!editing ? (
        <li className='flex items-center'>
          <input
            type='checkbox'
            checked={itemData.completed}
            onChange={() => handleChange(itemData.id)}
            className='form-checkbox h-6 w-6 text-blue-600'
          />
          <span
            className={`ml-2 ${
              itemData.completed ? 'line-through text-gray-400' : ''
            }`}
          >
            {itemData.title}
          </span>
        </li>
      ) : (
        <li className='flex items-center'>
          <input
            type='text'
            value={title}
            onChange={(e) => setTitle(e.target.value)}
            className='border-b border-gray-300 focus:outline-none'
          />
        </li>
      )}

      {/* the code is saying "if editing is false, render the 'Edit' button; otherwise, render the 'Update' and 'Cancel' buttons". */}
      <div className='flex  border-lime-500'>
        {!editing ? (
          <button
            onClick={handleEditing}
            className='mr-2
          border-lime-500 rounded-lg p-2 bg-lime-500 hover:bg-lime-700 text-white font-bold py-2 px-4
          '
          >
            <AiFillEdit />
          </button>
        ) : (
          <>
            <button
              onClick={handleUpdate}
              className='mr-2
            border-lime-500 rounded-lg p-2 bg-lime-500 hover:bg-lime-700 text-white font-bold py-2 px-4
            '
            >
              <MdUpdate />
            </button>
            <button
              onClick={handleCancel}
              className='mr-2
              border-lime-500 rounded-lg p-2 bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4
            '
            >
              <MdOutlineCancel />
            </button>
          </>
        )}
        <button
          onClick={() => handleDelete(itemData.id)}
          className='bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded-lg'
        >
          <FaTrash />
        </button>
      </div>
    </div>
  )
}

export default TodoItem

/*
  This code defines a React functional component called TodoItem that represents a single todo item in a list of todos. It takes two props: itemData which is an object containing information about the todo item, and setTodos which is a function to update the list of todos.

  The component has a local function called handleChange which is called when the user clicks on the checkbox associated with the todo item. This function takes an id parameter and updates the completed state of the corresponding todo item by toggling it. It does this by calling the setTodos function passed in as a prop, which receives a function that updates the previous state of the todos array by mapping over it and updating the completed state of the todo item with the matching id.

  The component renders a div containing a single list item with a checkbox and the title of the todo item. The checkbox is checked if the completed property of the itemData object is true. When the user clicks on the checkbox, it calls the handleChange function with the id of the corresponding todo item.

*/


// Editing Mode Added

/*

  The component now renders an input field when the editing state is true. When the user clicks on the "Edit" button, the editing state is set to true and the input field is displayed. The title of the to-do item is pre-populated in the input field, and the user can edit it. When the user clicks on the "Update" button, the title state is updated and the to-do item is updated in the todos array. When the user clicks on the "Cancel" button, the title state is reset to the original value and the input field is hidden.

 */