import { useState, useEffect } from 'react'
import dataList from '../data/data'
import InputTodo from './InputTodo'
import TodosList from './TodosList'

const TodosLogic = () => {
  const [todos, setTodos] = useState(getInitialTodos())

  function getInitialTodos() {
    // getting stored items
    const temp = localStorage.getItem('todoList')
    const savedTodos = JSON.parse(temp)
    return savedTodos || []
  }


  useEffect(() => {
    // storing todos items
    const temp = JSON.stringify(todos)
    localStorage.setItem('todoList', temp)
  }, [todos])

  return (
    <div className='bg-gray-100 flex flex-col items-center justify-center  w-full max-w-lg mx-auto'>
      <div className='max-w-lg mx-auto py-10 px-4 w-full'>
        <InputTodo todos={todos} setTodos={setTodos} />
        <TodosList todosProps={todos} setTodos={setTodos} />
      </div>
    </div>
  )
}

export default TodosLogic
