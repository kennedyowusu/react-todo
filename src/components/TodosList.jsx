import React from 'react'
import TodoItem from './TodoItem'

const TodosList = ({ todosProps, setTodos }) => {
  return (
    <div className='flex flex-col items-center justify-center mt-4'>
      <ul className='w-full max-w-lg'>
        {todosProps.map((item) => (
          <TodoItem key={item.id} itemData={item} setTodos={setTodos} />
        ))}
      </ul>
    </div>
  )
}

export default TodosList
