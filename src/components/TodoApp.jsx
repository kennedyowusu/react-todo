import Header from './Header'
import TodosLogic from './TodosLogic'

const TodoApp = () => {
  return (
    <div
      className='flex flex-col items-center justify-center min-h-screen bg-gray-100'>
      <Header />
      <TodosLogic />
    </div>
  )
}
export default TodoApp
