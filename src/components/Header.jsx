const Header = () => {
  return (
    <header className='bg-gray-200 p-4 
      rounded-b-lg shadow-md mb-4 w-full max-w-lg mx-auto
    '>
      <h1 className='text-4xl font-bold text-center text-teal-600'>Todos</h1>
      <p className='text-gray-700 text-center mt-2'>
        Enter your daily task and it will persist in the browser local storage
      </p>
    </header>
  )
}
export default Header
