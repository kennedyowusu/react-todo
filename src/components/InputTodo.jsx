import { useState } from 'react'
import { v4 as uuidv4 } from 'uuid'

const InputTodo = ({ todos, setTodos }) => {
  const [title, setTitle] = useState('')
  const [message, setMessage] = useState('')

  const addTodoItem = (title) => {
    const newTodo = {
      id: uuidv4(),
      title: title,
      completed: false,
    }
    setTodos([...todos, newTodo])
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    if (title.trim()) {
      addTodoItem(title)
      setTitle('')
    } else {
      setMessage('Please write item')
    }
  }

  const handleChange = (e) => {
    setTitle(e.target.value)
  }

  return (
    <div
      className='flex flex-col items-center justify-center w-full max-w-lg mx-auto
    '
    >
      <form onSubmit={handleSubmit} className='w-full max-w-lg mb-4'>
        <div className='flex items-center border-b border-teal-500 py-2'>
          <input
            type='text'
            value={title}
            placeholder='Add Todo...'
            onChange={handleChange}
            className='appearance-none bg-transparent border-none w-full text-gray-700 mr-3 py-1 px-2 leading-tight focus:outline-none'
          />
          <button
            type='submit'
            className='flex-shrink-0 bg-teal-500 hover:bg-teal-700 border-teal-500 hover:border-teal-700 text-sm border-4 text-white py-1 px-2 rounded'
          >
            Add New Item
          </button>
        </div>
      </form>
      {message && <span className='text-red-500'>{message}</span>}
    </div>
  )
}

export default InputTodo
